# Description

For this assignment, we would like to see a form component developed, containing an input component that consists of multiple input fields (e.g. postal code, license plate, money with cents etc). The following functionalities should be implemented:
- the form should be unaware of the component's multiple separate input fields, the input should be accessible as a single, combined value.
- the form should only be submittable when the combined value passes a self-chosen pattern / validation.
- basic input behaviour / state should apply (e.g. disabled, required).

Attached you will find a quickstart of the assignment, to eliminate the need to spend a lot of time on writing boilerplate code. This comes with a basic setup, configuration for stencil, unit tests and bdd.


# Goal

The goal of this assignment is to get a general understanding of your approach on component development and an idea of your creativity and meticulousness. There is no expectation for this assignment to take multiple days. We would like to see your current skillset, keep it simple and show what you've got. This assignment should not take more than 4-6 hours of your time.


# Requirements

- You use our Starter Pack
- Usage of StencilJS for component development.
- Apply proper component logic and behaviour, follow best practises.
- No usage of other frameworks or libraries.
- Pay attention to presentation / styling (styled form and fields).
- Minimal Unit / BDD test coverage (coverage with at least one test where applicable).
- Handle unexected user input.


# Guidelines

Remember this component could be implemented in a corporate environment, used throughout the organisation, so:
- Write readable, consistent code.
- Write basic documentation.
- Commit early, write proper commit messages. Resulting in clean history.
- Try to give an estimation on worked hours / time logging.

When stuck or presented with any questions, please do not hesitate to contact us by email or phone:
dennis.n.jamin@rabobank.nl - +31(0)6 2721 6071

# Developer Comments

My experience with unit tests is more focused on Vue and vanilla Javascript, so it was hard for me to get up to speed with the way to do it when working with stencil components.
For services I wrote normal unit tests (npm run test.u) whereas for components I used end to end (npm run test) because from what I understand to get the components mounted one has to use 
 he class newE2EPage

