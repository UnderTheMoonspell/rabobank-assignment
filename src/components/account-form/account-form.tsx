import { Component, State } from "@stencil/core";
import appConfig from "../../global/app.config";
import AccountService from "../../services/account.service";
import CustomMessage from "../../models/message";
import Account from "../../models/account";

@Component({
	tag: "account-form",
	styleUrl: "account-form.scss",
	shadow: true
})
export class AccountForm {
	private title: string = "Open Account";
	private accountService: AccountService = new AccountService();

	@State()
	formObject: Account = { ...appConfig.defaultFormObject };

	@State()
	currentCurrency: string = "€";

	@State()
	message: CustomMessage = new CustomMessage();

	inputChange(inputName, value) {
		this.formObject[inputName] = value;
	}

	toggleCurrency() {
		this.currentCurrency = this.currentCurrency === "€" ? "£" : "€";
	}

	handleSubmit(e: Event): void {
		e.preventDefault();
		this.accountService
			.createAccount(this.formObject)
			.then(message => {
				this.message = {
					...this.message,
					text: message,
					level: "success"
				};

				this.formObject = { 
					...new Account()
				};

				setTimeout(() => (this.message = { ...new CustomMessage() }), 3000);
			})
			.catch(err => {
				this.message = { ...this.message, text: err, level: "error" };
				setTimeout(() => (this.message = { ...new CustomMessage() }), 3000);
			});
	}

	render() {
		return (
			<div>
				{this.message.text ? (
					<alert-message message={this.message} />
				) : (
					""
				)}
				<form-header titleText={this.title} />
				<div class="currency-flag">
					<span>Currency</span>
					<img
						onClick={() => this.toggleCurrency()}
						src={
							this.currentCurrency === "€"
								? "../../assets/eu.jpg"
								: "../../assets/uk.jpg"
						}
					/>
				</div>
				<form onSubmit={e => this.handleSubmit(e)}>
					<custom-input
						label={appConfig.labels.email}
						value={this.formObject.email}
						name={appConfig.labels.email.toLowerCase()}
						required={true}
						type="email"
						onChanged={ev => this.inputChange("email", ev.detail)}
					/>

					<custom-input
						label={appConfig.labels.name}
						value={this.formObject.name}
						name={appConfig.labels.name.toLowerCase()}
						required={true}
						maxlength={100}
						type="text"
						onChanged={ev => this.inputChange("name", ev.detail)}
					/>

					<custom-input
						label={appConfig.labels.nationality}
						value={this.formObject.nationality}
						name={appConfig.labels.nationality.toLowerCase()}
						type="text"
						onChanged={ev =>
							this.inputChange("nationality", ev.detail)
						}
					/>

					<custom-input
						label={appConfig.labels.zipCode}
						value={this.formObject.zipCode}
						name={appConfig.labels.zipCode.toLowerCase()}
						maxlength={15}
						type="text"
						onChanged={ev => this.inputChange("zipCode", ev.detail)}
					/>

					<custom-input
						label={appConfig.labels.job}
						value={this.formObject.job}
						name={appConfig.labels.job.toLowerCase()}
						maxlength={100}
						type="text"
						onChanged={ev => this.inputChange("job", ev.detail)}
					/>

					<custom-input
						label={`${appConfig.labels.initialDeposit} (${
							this.currentCurrency
						})`}
						value={this.formObject.initialDeposit}
						name={appConfig.labels.initialDeposit.toLowerCase()}
						type="number"
						min={100}
						onChanged={ev =>
							this.inputChange("initialDeposit", ev.detail)
						}
					/>

					<custom-input
						name={appConfig.labels.submit.toLowerCase()}
						type="submit"
					/>
				</form>
			</div>
		);
	}
}
