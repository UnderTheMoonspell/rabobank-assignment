import { newE2EPage } from "@stencil/core/testing";
import { AccountForm } from "../../account-form";

describe("Account Form", function() {
	it("should build", function() {
		expect(new AccountForm()).toBeTruthy();
	});

	let page;
	let elm;

	beforeEach(async () => {
		page = await newE2EPage({ html: "<account-form></account-form>" });
		elm = await page.find("account-form");
	});

	it("should render", async () => {
		expect(elm).not.toBeNull();
	});

	it("should render correctly", async () => {
		const currencyFlag = await page.find("account-form >>> .currency-flag");
		expect(currencyFlag).toBeTruthy();
	});

	it("should change currency", async () => {
		await elm.setProperty("currentCurrency", "€");
		await page.waitForChanges();

		const flagImage = await page.find("account-form >>> img");
		expect(flagImage.outerHTML).toContain("eu.jpg");
		await flagImage.click();
		expect(flagImage.outerHTML).toContain("uk.jpg");
	});

	//Not working
	it("should submit form", async () => {
		const spy = jest.fn();
		await elm.setProperty("handleSubmit", spy);
		await page.waitForChanges();

		const submitButton = await page.find("account-form >>> #submit");
		expect(submitButton).toBeDefined();
		await submitButton.click();
		expect(spy).toHaveBeenCalled();
	});
});
