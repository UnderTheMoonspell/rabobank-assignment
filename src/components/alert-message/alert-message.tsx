import { Component, Prop } from '@stencil/core';
// import CustomMessage from '../models/message';


@Component({
  tag: 'alert-message',
  styleUrl: 'alert-message.scss',
  scoped: true,
  shadow: true
})
export class AlertMessage {
  @Prop()
  message: any;

  render() {
    return (
      <div
        class={"alert " + (this.message.level.toLowerCase() === 'success' ? 'success' : 'error')}>
        <div class="message">{this.message.text}</div>
      </div>
    )
  }
}