import { AlertMessage } from "../../alert-message";
import { newE2EPage } from "@stencil/core/testing";
// import Message from "../../../models/message";

describe("Alert Message", function() {
	it("should build", function() {
		expect(new AlertMessage()).toBeTruthy();
	});

	let page;
	let elm;
	let message = { text: 'test', level: 'success' };
	
	beforeEach(async () => {
		page = await newE2EPage({ html: "<alert-message></alert-message>" });
		elm = await page.find("alert-message");
	});

	it("should render", async () => {
		expect(elm).not.toBeNull();
	});


	// Form some reason it does not work
	it.skip("should render correctly", async () => { 
		await elm.setProperty('message', message);
		await page.waitForChanges();

		const alertMessage = await page.find(".message");
		expect(alertMessage.innerHTML).toEqual(message.text);
	});
});
