import { Component } from '@stencil/core';


@Component({
  tag: 'custom-footer',
  styleUrl: 'custom-footer.scss',
  scoped: true,
  shadow: true
})
export class CustomFooter {

  render() {
    return (
      <div class="footer">
        <h5>Developed by Bernardo Azevedo</h5>
      </div>
    )
  }
}