import { Component, Prop, EventEmitter, Event } from "@stencil/core";

@Component({
	tag: "custom-input",
	styleUrl: "custom-input.css",
	scoped: true
})
export class CustomInput {
	@Prop() label: string;
	@Prop() value: any;
	@Prop() type: string;
	@Prop() name: string;
	@Prop() required: boolean;
	@Prop() min: number;
	@Prop() maxlength: number;
	@Event() changed: EventEmitter;

	handleChange(e): void {
		this.changed.emit(e.target.value);
	}

	render() {
		return (
			<div class="input-container">
				<label>
					{this.label} {this.required ? "*" : ""}
				</label>
				<input
					id={this.name}
					name={this.name}
					type={this.type}
					value={this.value}
					required={this.required ? true : false}
					min={this.min ? this.min : null}
					maxlength={this.maxlength ? this.maxlength : null}
					onInput={e => this.handleChange(e)}
				/>
			</div>
		);
	}
}
