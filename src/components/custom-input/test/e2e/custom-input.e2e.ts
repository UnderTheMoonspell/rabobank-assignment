import { CustomInput } from "../../custom-input";
import { newE2EPage } from "@stencil/core/testing";

describe("Custom Message", function() {
	it("should build", function() {
		expect(new CustomInput()).toBeTruthy();
	});

	let page;
	let elm;

	beforeEach(async () => {
		page = await newE2EPage({ html: "<custom-input></custom-input>" });
		elm = await page.find("custom-input");
	});

	it("should render", async () => {
		expect(elm).not.toBeNull();
	});

	it("should render correctly", async () => {
		const customInput = await page.find("input");
		expect(customInput).toBeTruthy();
	});

	it("should change value", async () => {
		const customInput = await page.find("input");
		let value = await customInput.getProperty("value");
		expect(value).toBe("");

		await customInput.press("8");
		value = await customInput.getProperty("value");
		expect(value).toBe("8");
	});	
});
