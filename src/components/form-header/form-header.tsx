import { Component, Prop } from '@stencil/core';


@Component({
  tag: 'form-header',
  styleUrl: 'form-header.scss',
  shadow: true
})
export class FormHeader {

  @Prop() titleText: string;

  render() {
    return (
      <div>
        <h1 class="form-title">{this.titleText}</h1>
      </div>
    )
  }
}