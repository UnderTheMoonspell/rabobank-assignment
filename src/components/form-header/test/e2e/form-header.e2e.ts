import { FormHeader } from "../../form-header";
import { newE2EPage } from "@stencil/core/testing";

describe("Form Header", function() {
	it("should build", function() {
		expect(new FormHeader()).toBeTruthy();
	});

	let page;
	let elm;
	let title = "RaboBank Assignment";

	beforeEach(async () => {
		page = await newE2EPage({ html: "<form-header></form-header>" });
		elm = await page.find("form-header");
	});

	it("should render", async () => {
		expect(elm).not.toBeNull();
	});

	it("should render title correctly", async () => {
		await elm.setProperty("titleText", title);
		await page.waitForChanges();

		const headerTitle = await page.find("form-header >>> .form-title");
		expect(headerTitle.innerHTML).toEqual(title);
	});
});
