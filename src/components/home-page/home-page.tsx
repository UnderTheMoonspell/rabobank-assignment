import { Component } from '@stencil/core';


@Component({
  tag: 'home-page',
  styleUrl: 'home-page.css'
})
export class HomePage {
  render() {
    return (
      <div>
        <div>
          <img src="../../assets/logo.png" />
        </div>
        <account-form></account-form>
        <custom-footer></custom-footer>
      </div>
    )
  }
}