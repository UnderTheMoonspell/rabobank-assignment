const baseURL = "http://localhost/api";

export default {
	labels: {
		email: "Email",
		submit: "Submit",
		zipCode: "Zip Code",
		name: "Name",
		nationality: "Nationality",
		initialDeposit: "Initial Deposit",
		job: "Job"
	},
	messages: {
		accountCreateSuccess: "Account created successfully",
		accountCreateError: "Error creating account"
	},

	APIURL: {
		createAccount: `${baseURL}/account`
	},

	defaultFormObject: {
		email: "",
		initialDeposit: 0,
		name: "",
		nationality: "Portugal",
		zipCode: "",
		job: "Hopefully Rabobank's Front End Developer"
	}
};
