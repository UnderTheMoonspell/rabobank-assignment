export default class Account {
  name: string;
  nationality: string;
  email: string;
  zipCode: string;
  initialDeposit: number;
  job: string;
}