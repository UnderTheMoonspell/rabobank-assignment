
import DataService from "./data.service";
import Account from "../models/account";
import appConfig from "../global/app.config";

export default class AccountService {
	private dataService = new DataService();

	createAccount(accountForm: Account): Promise<string> {
		return this.dataService.post(appConfig.APIURL.createAccount, accountForm);
	}
}
