import appConfig from "../global/app.config";

export default class DataService {

  get(url) {
    console.log(`Call axios get method ${url}`);
  }

  post(url, payload) : Promise<string> {
    console.log(`Call axios post method ${url} and ${JSON.stringify(payload)}`);
    // alert(appConfig.messages.accountCreateSuccess);
    return Promise.resolve(appConfig.messages.accountCreateSuccess);
  }
}