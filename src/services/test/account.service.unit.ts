import AccountService from "../account.service";
import DataService from "../data.service";
import Account from "../../models/account";
import appConfig from "../../global/app.config";
jest.mock('../data.service');

describe("Account Service", function() {
	it("should create instance", function() {
		expect(new AccountService()).toBeTruthy();
	});

	it("should call data service", function() {
		const service = new AccountService();
		const newObj = new Account();
		service.createAccount(newObj);
		expect(DataService.prototype.post).toHaveBeenCalledWith(appConfig.APIURL.createAccount, newObj);
	});	
})