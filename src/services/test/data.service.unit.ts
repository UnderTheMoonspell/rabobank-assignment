import DataService from "../data.service";

describe("Data Service", function() {
	it("should create instance", function() {
		expect(new DataService()).toBeTruthy();
	});

	it("should call post method", function() {
		const service = new DataService();
		const spy = jest.spyOn(service, 'post').mockImplementation(() => Promise.resolve('Success'));
		service.post('lala', {});
		expect(spy).toHaveBeenCalled();
	});	
})